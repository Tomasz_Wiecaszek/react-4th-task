import React, {createContext, useState} from 'react';
export const GameState=createContext({
isName:false,
setName:item=>{}

})
export const CharStats=createContext({
    name:"",
    str:0,
    hp:0,
    speed:0,
    dmg:0,
    lvl:0,
    addStr:item=>{},
    addName:item=>{}
})
export const GameApp=({children})=>{
const [isName,setName]=useState(false);
return(
    <GameState.Provider value={{isName,setName}}>
        {children}
    </GameState.Provider>
)
}

export const UserStatsProvider=({children})=>{
    const [name,addName]=useState("");
    const [str,addStr]=useState(10);
    const hp=10;
    const speed=2;
    const dmg=speed*str;
    const lvl=1;

    return(
        <CharStats.Provider
        value={{
            name,
            addName,
            str,
            addStr,
            hp,
            speed,
            dmg,
            lvl
            
            
        }}
        >
            {children}
        </CharStats.Provider>
    )
}

export const OponentStatsProvider=({children})=>{
    const name="Oponent";
    const str=10;
    const hp=12;
    const speed=2;
    const dmg=speed*str;
    const lvl=1;

    return(
        <CharStats.Provider
        value={{
            name,
            str,
            hp,
            speed,
            dmg,
            lvl
        }}
        >
            {children}
        </CharStats.Provider>
    )
}