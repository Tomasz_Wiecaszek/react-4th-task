import React,{useContext,useState} from "react";
import {GameState} from "../../context"


const Form=()=>{
const {isName,setName}=useContext(GameState);
const [name,addName]=useState('');
const handleSubmit=(e)=>{
    e.preventDefault();
    setName(true);
    localStorage.setItem("name",name);
}
return(
<form onSubmit={e=>handleSubmit(e)}>
    <label>Imię: </label>
<input name="name" type="text" value={name} onChange={(e)=>addName(e.target.value)} />
<input name="send" type="submit"/>
</form>

)

}

export default Form;