import './style.css'
import TableRows from '../../components/TableRows'
import {CharStats} from "../../context"
import React, {useContext, useEffect} from 'react'

const Gamer =({alive})=>{
    let {name,str,hp,speed,dmg,addName,lvl}=useContext(CharStats);
    const myStats={name,str,hp,speed,dmg,lvl,alive};
useEffect(()=>{
    if(!alive){
    addName(localStorage.getItem("name"));}
},);
    return(
<div className="col-6">
    <TableRows {...myStats}/>
    </div>
    )}

export default Gamer;