import './style.css'
import {CharStats} from "../../context"
import React,{useContext} from 'react'
import TableRows from "../../components/TableRows"
const Oponent =(props)=>{
    const {name,str,hp,speed,dmg,lvl}=useContext(CharStats);
    const myStats={name,str,hp,speed,dmg,lvl};
    return(
<div className="col-6">
    {props.alive?
        <TableRows {...myStats}/>
    :<div>Deaddd</div>}
</div>
    )
}
export default Oponent;