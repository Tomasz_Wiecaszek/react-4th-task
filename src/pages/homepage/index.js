import User from "../../components/User";
import Oponent from "../../components/Oponent";
import Gamer from "../../components/Gamer";
import './style.css';
import {
  UserStatsProvider,
  OponentStatsProvider,
  GameState,
} from "../../context";
import Form from "../../components/Form";
import { useContext, useEffect } from "react";

const Homepage = () => {
  const { isName, setName } = useContext(GameState);
  useEffect(() => {
    console.log(isName);
    const getName = localStorage.getItem("name");
    if (getName && getName.length > 0) {
      setName(true);
    }
  }, [isName]);
  return (
    <div>
      {isName ? (
        <div className="main-wrapper">
          <UserStatsProvider>
            <Gamer />
          </UserStatsProvider>

          <OponentStatsProvider>
            <Gamer alive />
          </OponentStatsProvider>
        </div>
      ) : (
        <Form />
      )}
    </div>

    // Tu skończyłem
  );
};

export default Homepage;
